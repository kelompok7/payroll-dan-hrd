<?php

require_once 'model/ContactsGateway.php';
require_once 'model/ValidationException.php';


class ContactsService {
    
    private $contactsGateway    = NULL;
    
    private function openDb() {
        if (!mysql_connect("localhost", "root", "")) {
            throw new Exception("Connection to the database server failed!");
        }
        if (!mysql_select_db("hotela")) {
            throw new Exception("No mvc-crud database found on database server.");
        }
    }
    
    private function closeDb() {
        mysql_close();
    }
  
    public function __construct() {
        $this->contactsGateway = new ContactsGateway();
    }
    
    public function getAllContacts($showby,$cari) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->selectAll($showby,$cari);
            $this->closeDb();
            return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
    }
	public function loginStaff($Nama_Pengguna,$Kata_Sandi,$report,$id) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->selectForLogin($Nama_Pengguna,$Kata_Sandi,$report,$id);
            $this->closeDb();
            return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
		
    }
     public function getPosisi($id_divisi) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->selectPosisiByDivisi($id_divisi);
            $this->closeDb();
            return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
        return $this->contactsGateway->find($id);
    }
    public function getStaff($id,$status) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->selectById($id,$status);
            $this->closeDb();
            return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
        return $this->contactsGateway->find($id);
    }
	 public function getStaff1($id) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->selectById1($id);
            $this->closeDb();
            return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
        return $this->contactsGateway->find($id);
    }
    
    private function validateContactParams( $Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi ) {
        $errors = array();
        if ( !isset($Nama_Pengguna) || empty($Nama_Pengguna) || !isset($Nama_Lengkap) || empty($Nama_Lengkap)
				|| !isset($Kata_Sandi) || empty($Kata_Sandi) || !isset($Tanggal_Lahir) || empty($Tanggal_Lahir)|| !isset($Jenis_Kelamin) || empty($Jenis_Kelamin) || !isset($Alamat) || empty($Alamat)||!isset($Divisi) || empty($Divisi)||!isset($Posisi) || empty($Posisi)) {
            $errors[] = 'Please complete the data.';
        }
        if ( empty($errors) ) {
            return;
        }
		var_dump(empty($Divisi));
        throw new ValidationException($errors);
    }
    
    public function createNewContact( $Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi ) {
        try {
            $this->openDb();
            $this->validateContactParams($Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi);
            $res = $this->contactsGateway->insert($Nama_Pengguna, $Nama_Lengkap, $Kata_Sandi, $Tanggal_Lahir,$Jenis_Kelamin,$Alamat,$Divisi,$Posisi);
            $this->closeDb();
            return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
    }
	 public function createNewSallary( $id,$deskripsi,$gaji) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->insertSallary($id,$deskripsi,$gaji);
            $this->closeDb();
            
			return $res;
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
    }
    
    public function deleteGaji( $id ) {
        try {
            $this->openDb();
            $res = $this->contactsGateway->delete($id);
            $this->closeDb();
        } catch (Exception $e) {
            $this->closeDb();
            throw $e;
        }
    }
    
    
}

?>
