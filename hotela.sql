-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2016 at 10:13 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotela`
--

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE IF NOT EXISTS `divisi` (
  `ID` mediumint(8) unsigned NOT NULL,
  `Nama_Divisi` varchar(50) DEFAULT NULL,
  `Deskripsi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`ID`, `Nama_Divisi`, `Deskripsi`) VALUES
(1, ' Marketing Departemen', 'Markets several hotels to the marketplace according to their needs '),
(2, 'Front Office Departemen', 'Sell ??rooms are fully qualified and ready to be occupied by hotel guests .'),
(3, 'Housekeeping Departemen ', 'Providing a clean and ready for occupation by hotel guests .'),
(4, 'Enggineering & Maintenance Departemen', 'Mengoperasikan, merawat, dan memperbaiki semua peralatan dalam hotel.'),
(5, 'Laundry Departemen', 'Membantu departemen Housekeeping dalam menyediakan kebutuhan Linen (Handuk, Seprai, Selimut) untuk kamar hotel dan seragam karyawan.'),
(6, 'Food & Beverage Departemen ', 'Menyiapkan makanan dan minuman di dalam hotel.'),
(7, 'Finance Departemen ', 'Mengelola keuangan baik pemasukan maupun pengeluaran hotel.'),
(8, 'Personnel Departemen ', 'Mengurusi seluruh adsministrasi  karyawan hotel.'),
(9, 'Training Departemen ', 'Memberikan berbagai latihan bagi karyawan hotel baik yang baru maupun yang lama.'),
(10, 'Security Departemen', 'Menjaga dan mengatur sistem keamanan hotel');

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE IF NOT EXISTS `gaji` (
  `ID` mediumint(9) unsigned NOT NULL,
  `Tambahan_Gaji` int(11) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Tanggal_Pembayaran` datetime(1) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Id_Karyawan` mediumint(6) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`ID`, `Tambahan_Gaji`, `Deskripsi`, `Tanggal_Pembayaran`, `Status`, `Id_Karyawan`) VALUES
(1, 5000000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-04 03:07:08.1', b'1', 1),
(2, 3000000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-07 07:19:19.3', b'1', 2),
(3, 1500000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-08 04:09:08.1', b'1', 3),
(4, 4000000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-01 11:49:36.0', b'1', 4),
(5, 1500000, 'Gaji diberikan dalam jangka waktu sekali dalam sebulan', '2016-06-01 11:49:10.0', b'1', 5);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `ID` mediumint(6) unsigned NOT NULL,
  `Nama_Pengguna` varchar(12) NOT NULL,
  `Nama_Lengkap` varchar(50) NOT NULL,
  `Kata_Sandi` varchar(60) NOT NULL,
  `Tanggal_Lahir` date NOT NULL,
  `Jenis_Kelamin` enum('male','female') NOT NULL,
  `Alamat` text NOT NULL,
  `Anggota_sejak` date NOT NULL,
  `forget_key` char(5) NOT NULL,
  `status` bit(1) NOT NULL,
  `Id_Posisi` mediumint(6) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`ID`, `Nama_Pengguna`, `Nama_Lengkap`, `Kata_Sandi`, `Tanggal_Lahir`, `Jenis_Kelamin`, `Alamat`, `Anggota_sejak`, `forget_key`, `status`, `Id_Posisi`) VALUES
(1, 'hetlysaint', 'Hetly Saint Kartika', 'saint002', '1996-06-24', 'female', 'jalan setia budi pasar 1 medan', '2008-05-04', '0', b'0', 1),
(2, 'Wendys', 'Wendy Winata', 'winata001', '1996-01-11', 'male', 'jalan dr mansur no.2a', '2009-09-07', '0', b'1', 2),
(3, 'aggieee', 'Aggie Wicita', 'wicita', '1996-10-09', 'female', 'jalan pembangunan no 8a medan', '2007-09-08', '0', b'0', 3),
(4, 'samuelezzay', 'Ezzay Tarigan', 'ezzay09', '1995-10-05', 'male', 'jalan berdikari no.99', '2008-01-01', '0', b'0', 4),
(5, 'okisalsa22', 'Oki Salsabilla', '123456', '1995-07-30', 'female', 'Jalan Pembangunan No.7', '2014-01-01', '0', b'0', 5);

-- --------------------------------------------------------

--
-- Table structure for table `posisi`
--

CREATE TABLE IF NOT EXISTS `posisi` (
  `ID` mediumint(6) unsigned NOT NULL,
  `Nama_Posisi` varchar(50) NOT NULL,
  `Akses_Level` tinyint(4) NOT NULL,
  `Gaji_Pokok` int(11) unsigned NOT NULL,
  `Id_Divisi` mediumint(9) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posisi`
--

INSERT INTO `posisi` (`ID`, `Nama_Posisi`, `Akses_Level`, `Gaji_Pokok`, `Id_Divisi`) VALUES
(1, 'Manager', 2, 7000000, 1),
(2, 'Admin', 1, 6000000, 7),
(3, 'Member', 0, 5000000, 1),
(4, 'Manager', 2, 7000000, 3),
(5, 'Member', 0, 4000000, 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `seluruh`
--
CREATE TABLE IF NOT EXISTS `seluruh` (
`ID` mediumint(6) unsigned
,`Nama_Pengguna` varchar(12)
,`Nama_Lengkap` varchar(50)
,`Kata_Sandi` varchar(60)
,`Tanggal_Lahir` date
,`Jenis_Kelamin` enum('male','female')
,`Alamat` text
,`Anggota_sejak` date
,`forget_key` char(5)
,`status` bit(1)
,`Id_Posisi` mediumint(6) unsigned
,`Nama_Posisi` varchar(50)
,`Akses_Level` tinyint(4)
,`Gaji_Pokok` int(11) unsigned
,`Id_Divisi` mediumint(9) unsigned
,`Nama_Divisi` varchar(50)
,`Deskripsi` text
,`Id_gaji` mediumint(9) unsigned
,`Tambahan_Gaji` int(11)
,`Des_Gaji` text
,`Tanggal_Pembayaran` datetime(1)
,`Status_gaji` bit(1)
);

-- --------------------------------------------------------

--
-- Structure for view `seluruh`
--
DROP TABLE IF EXISTS `seluruh`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `seluruh` AS (select `a`.`ID` AS `ID`,`a`.`Nama_Pengguna` AS `Nama_Pengguna`,`a`.`Nama_Lengkap` AS `Nama_Lengkap`,`a`.`Kata_Sandi` AS `Kata_Sandi`,`a`.`Tanggal_Lahir` AS `Tanggal_Lahir`,`a`.`Jenis_Kelamin` AS `Jenis_Kelamin`,`a`.`Alamat` AS `Alamat`,`a`.`Anggota_sejak` AS `Anggota_sejak`,`a`.`forget_key` AS `forget_key`,`a`.`status` AS `status`,`a`.`Id_Posisi` AS `Id_Posisi`,`b`.`Nama_Posisi` AS `Nama_Posisi`,`b`.`Akses_Level` AS `Akses_Level`,`b`.`Gaji_Pokok` AS `Gaji_Pokok`,`b`.`Id_Divisi` AS `Id_Divisi`,`c`.`Nama_Divisi` AS `Nama_Divisi`,`c`.`Deskripsi` AS `Deskripsi`,`d`.`ID` AS `Id_gaji`,`d`.`Tambahan_Gaji` AS `Tambahan_Gaji`,`d`.`Deskripsi` AS `Des_Gaji`,`d`.`Tanggal_Pembayaran` AS `Tanggal_Pembayaran`,`d`.`Status` AS `Status_gaji` from (((`karyawan` `a` join `posisi` `b`) join `divisi` `c`) join `gaji` `d`) where ((`a`.`Id_Posisi` = `b`.`ID`) and (`a`.`ID` = `d`.`Id_Karyawan`) and (`b`.`Id_Divisi` = `c`.`ID`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Id_Karyawan` (`Id_Karyawan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Id_Posisi` (`Id_Posisi`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Id_Divisi` (`Id_Divisi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
  MODIFY `ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `ID` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `ID` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `ID` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gaji`
--
ALTER TABLE `gaji`
  ADD CONSTRAINT `gaji_ibfk_1` FOREIGN KEY (`Id_Karyawan`) REFERENCES `karyawan` (`ID`);

--
-- Constraints for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `karyawan_ibfk_1` FOREIGN KEY (`Id_Posisi`) REFERENCES `posisi` (`ID`);

--
-- Constraints for table `posisi`
--
ALTER TABLE `posisi`
  ADD CONSTRAINT `posisi_ibfk_1` FOREIGN KEY (`Id_Divisi`) REFERENCES `divisi` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
